#pragma once

#include <string>
#include <deque>

#include "Change.h"

class QueryGenerator
{
    public:
        QueryGenerator();
        virtual ~QueryGenerator();

        virtual std::string generateQuery(const std::deque<PathNode> queryPath) = 0;

    protected:
        std::string getRelFilter(const std::string &relExpr, const PathNode::Type &relDirection, bool reverse);

};

class DeltaQueryGenerator : public QueryGenerator
{
    public:
        DeltaQueryGenerator();
        ~DeltaQueryGenerator();

        std::string generateQuery(const std::deque<PathNode> queryPath) override;

        void preparePrefix(std::string &query, const std::deque<PathNode> &updatePath, int r, int numNodes);
        void prepareSuffix(std::string &query, const std::deque<PathNode> &updatePath, int l, int numNodes);

};

class LocalisedQueryGenerator : public QueryGenerator
{
    public:
        LocalisedQueryGenerator(const std::vector<int> &proj);
        ~LocalisedQueryGenerator();

        std::string generateQuery(const std::deque<PathNode> queryPath) override;

        void preparePrefix(std::string &query, const std::deque<PathNode> &updatePath, int r, int numNodes);
        void prepareSuffix(std::string &query, const std::deque<PathNode> &updatePath, int l, int numNodes);

    private:
        const std::vector<int> &proj;

};