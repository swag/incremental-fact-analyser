### Python3 equivalent of running Neo4j Queries. Used to set up Analysis Results
### before running Incremental Analysis
###
### Make sure both Neo4j and Neo4j python drivers are installed

from neo4j import GraphDatabase
import sys
import os

if __name__ == "__main__":
    
    if len(sys.argv) < 5:
        print ("Usage: python3 " + sys.argv[0] + " <neo4j_userName> <neo4j_password> <script.cypher> <numCols>")
        exit(1)
    uri = "bolt://localhost:7687"
    driver = GraphDatabase.driver(uri, auth=(sys.argv[1], sys.argv[2]))
    query = open(os.path.abspath(sys.argv[3]), "r").read()
    numCols = int(sys.argv[4])
    createIndex = "Create index on :DerivedFact("
    for i in range(1, numCols+1):
        if i < numCols:
            createIndex += "n{0},".format(i)
        else:
            createIndex += "n{0})".format(i)
    with driver.session() as session:
        session.run(createIndex)
        session.run(query)
    
    driver.close()