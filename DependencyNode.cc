#include "DependencyNode.h"
#include "Change.h"
#include <iostream> 
#include <algorithm>


using namespace std;

DependencyNode::DependencyNode(string aLabel) : leftChild{nullptr}, rightChild{nullptr}, label{aLabel} {}
DependencyNode::DependencyNode(string aLabel, vector<int> cols) : leftChild{nullptr}, rightChild{nullptr}, label{aLabel}, cols{cols} {}
DependencyNode::~DependencyNode()
{
    if(leftChild)
    {
        delete leftChild;
    }
    
    if(rightChild)
    {
        delete rightChild;
    }
}

void DependencyNode::setLeftChild(DependencyNode *left)
{
    leftChild = left;
}

void DependencyNode::setRightChild(DependencyNode *left)
{
    rightChild = left;
}

string DependencyNode::getLabel()
{
    return label;
}

DependencyNode* DependencyNode::getLeftChild() const
{
    return leftChild;
}

DependencyNode* DependencyNode::getRightChild() const
{
    return rightChild;
}

JoinNode::JoinNode(string aLabel) : DependencyNode(aLabel){}

UnionNode::UnionNode(string aLabel) : DependencyNode(aLabel){}

TransClosureNode::TransClosureNode(string aLabel) : DependencyNode(aLabel) {}

ProjectionNode::ProjectionNode(string aLabel, vector<int> cols) : DependencyNode(aLabel, cols) {}

InverseNode::InverseNode(string aLabel) : DependencyNode(aLabel) {}

BaseFact::BaseFact(string relName) : DependencyNode(relName), relName{relName}{}

Change JoinNode::propogateChange(neo4j_connection_t *connection) const
{
    Change leftChange = leftChild->propogateChange(connection);
    Change rightChange = rightChild->propogateChange(connection);
    
    int newNumCols = leftChange.getNumHeadings() + rightChange.getNumHeadings() - 1;
    int leftNumCols = leftChange.getNumHeadings();
    int rightNumCols = rightChange.getNumHeadings();
    DataItem toAdd;
    toAdd.id = "?";
    while(leftNumCols < newNumCols)
    {
        leftChange.appendToPatterns(toAdd);
        leftNumCols++;
    }
    while(rightNumCols < newNumCols)
    {
        rightChange.prependToPatterns(toAdd);
        rightNumCols++;
    }
    leftChange.join(rightChange);
    return leftChange;
}

Change UnionNode::propogateChange(neo4j_connection_t *connection) const
{
    //~ cout << getSymbol() << endl;
    Change leftChange = leftChild->propogateChange(connection);
    Change rightChange = rightChild->propogateChange(connection);
    
    leftChange._union(rightChange);
    
    return leftChange;
}

Change TransClosureNode::propogateChange(neo4j_connection_t *connection) const
{
    //~ cout << getSymbol() << endl;
    Change leftChange = leftChild->propogateChange(connection);
    //~ Change result = rightChange;
    
    
    for(Row &it : leftChange.getAdditionPatterns())
    {
        it[0].transType = DataItem::TransType::SRC;
        it[it.size() - 1].transType = DataItem::TransType::DST;
        it.updateQuery = "($)-[:" + leftChange.getHeading(leftChange.getNumHeadings() - 1).id + "*0..]->" 
                        + it.updateQuery + "-[:" + leftChange.getHeading(leftChange.getNumHeadings() - 1).id + "*0..]->($)";
                        
        PathNode srcNode, edgeNode;
        srcNode.type = PathNode::NODE;

        edgeNode.id = leftChange.getHeading(leftChange.getNumHeadings() - 1).id;
        edgeNode.type = PathNode::EDGE_RIGHT;
        edgeNode.minLength = 0;
        
        it.updatePath.push_front(edgeNode);
        it.updatePath.push_front(srcNode);
        
        it.updatePath.push_back(edgeNode);
        it.updatePath.push_back(srcNode);
                        
        it.prefixQuery = "($)-[:" + leftChange.getHeading(leftChange.getNumHeadings() - 1).id + "*0..]->" + it.prefixQuery;
        it.suffixQuery = it.suffixQuery + "-[:" + leftChange.getHeading(leftChange.getNumHeadings() - 1).id + "*0..]->($)";
    }
    for(Row &it : leftChange.getRemovalPatterns())
    {
        it[0].transType = DataItem::TransType::SRC;
        it[it.size() - 1].transType = DataItem::TransType::DST;
        it.updateQuery = "($)-[:" + leftChange.getHeading(leftChange.getNumHeadings() - 1).id + "*0..]->" 
                            + it.updateQuery + "-[:" + leftChange.getHeading(leftChange.getNumHeadings() - 1).id + "*0..]->($)";
                            
        PathNode srcNode, edgeNode;
        srcNode.type = PathNode::NODE;

        edgeNode.id = leftChange.getHeading(leftChange.getNumHeadings() - 1).id;
        edgeNode.type = PathNode::EDGE_RIGHT;
        edgeNode.minLength = 0;
        
        it.updatePath.push_front(edgeNode);
        it.updatePath.push_front(srcNode);
        
        it.updatePath.push_back(edgeNode);
        it.updatePath.push_back(srcNode);
                            
                            
        it.prefixQuery = "($)-[:" + leftChange.getHeading(leftChange.getNumHeadings() - 1).id + "*0..]->" + it.prefixQuery;
        it.suffixQuery = it.suffixQuery + "-[:" + leftChange.getHeading(leftChange.getNumHeadings() - 1).id + "*0..]->($)";
    }
    
    
    //~ result.addAll(rightChange);
    Heading newHeading = leftChange.getHeading(leftChange.getNumHeadings() - 1);
    newHeading.id = newHeading.id + "*"; 
    leftChange.setHeading(leftChange.getNumHeadings() - 1, newHeading);
    return leftChange;
}

Change ProjectionNode::propogateChange(neo4j_connection_t *connection) const
{
    Change rightChange = rightChild->propogateChange(connection);
    
    for(Row &r : rightChange.getAdditionPatterns())
    {
        Row tmp;
        for(int col : cols)
        {
            tmp = tmp + r[col];
        }
        r.copyPattern(tmp);
    }
    
    
    for(Row &r : rightChange.getRemovalPatterns())
    {
        Row tmp;
        for(int col : cols)
        {
            tmp = tmp + r[col];
        }
        r.copyPattern(tmp);
    }
    
    return rightChange;
    
}

Change InverseNode::propogateChange(neo4j_connection_t *connection) const
{
    Change change = rightChild->propogateChange(connection);
    for(Row &r : change.getAdditionPatterns())
    {
        r.reverse();
    }
    
    for(Row &r : change.getRemovalPatterns())
    {
        r.reverse();
    }
    
    change.reverseHeading();
    
    return change;
    
}


Change BaseFact::propogateChange(neo4j_connection_t *connection) const
{
    Change result;
    
    // Interaction with FactBase
    string query = "MATCH (e:EdgeDiff) WHERE e.edgeType=\'" + getSymbol() + "\' AND e.dirty=true return e.src, e.dst";
    neo4j_result_stream_t *results =
            neo4j_run(connection, query.c_str(), neo4j_null);
    
    //~ if (results == NULL)
    //~ {
        //~ neo4j_perror(stderr, errno, "Failed to run statement");
        //~ return EXIT_FAILURE;
    //~ }
    

    neo4j_result_t *diff = neo4j_fetch_next(results);
    while (diff != NULL)
    {
        neo4j_value_t src = neo4j_result_field(diff, 0);
        neo4j_value_t dst = neo4j_result_field(diff, 1);
        char buf[2048];
        neo4j_string_value(src, buf, sizeof(buf));
        string srcString(buf);
        neo4j_string_value(dst, buf, sizeof(buf));
        string dstString(buf);
        // if(srcString.find("bvec") == string::npos && dstString.find("bvec") == string::npos){
        // if(srcString.find("vfree") == string::npos && srcString.find("user_path") == string::npos 
		// && srcString.find("fsnotify") == string::npos && srcString.find("oom_kill_process(") == string::npos
        //         && dstString.find("vfree") == string::npos && dstString.find("user_path") == string::npos 
		// && dstString.find("fsnotify") == string::npos && dstString.find("oom_kill_process(") == string::npos)
        // {
            DataItem d1, d2;
            d1.id = srcString;
            d2.id = dstString;
            Row r;
            r = r + d1;
            r = r + d2;
            r.updateQuery = "($ {id:\"" + srcString + "\"})-[:" + getSymbol() + "]->($ {id:\"" + dstString + "\"})";
            
            PathNode srcNode, dstNode, edgeNode;
            srcNode.id = srcString;
            srcNode.type = PathNode::NODE;
            
            dstNode.id = dstString;
            dstNode.type = PathNode::NODE;
            
            edgeNode.id = getSymbol();
            edgeNode.type = PathNode::EDGE_RIGHT;
            
            r.updatePath.push_back(srcNode);
            r.updatePath.push_back(edgeNode);
            r.updatePath.push_back(dstNode);
            
            r.prefixQuery = "($ {id:\"" + srcString + "\"})";
            r.suffixQuery = "($ {id:\"" + dstString + "\"})";
            result.addAddition(r);
        // }
        diff = neo4j_fetch_next(results);
    }
    query = "MATCH (e:EdgeDiff) WHERE e.edgeType=\'" + getSymbol() + "\' AND e.remove=true return e.src, e.dst";
    results = neo4j_run(connection, query.c_str(), neo4j_null);
    diff = neo4j_fetch_next(results);
    while (diff != NULL)
    {
        neo4j_value_t src = neo4j_result_field(diff, 0);
        neo4j_value_t dst = neo4j_result_field(diff, 1);
        char buf[2048];
        neo4j_string_value(src, buf, sizeof(buf));
        string srcString(buf);
        neo4j_string_value(dst, buf, sizeof(buf));
        string dstString(buf);
        // if(srcString.find("bvec") == string::npos && dstString.find("bvec") == string::npos){
        // if(srcString.find("vfree") == string::npos && srcString.find("user_path") == string::npos 
		// && srcString.find("fsnotify") == string::npos && srcString.find("oom_kill_process(") == string::npos
        //         && dstString.find("vfree") == string::npos && dstString.find("user_path") == string::npos 
		// && dstString.find("fsnotify") == string::npos && dstString.find("oom_kill_process(") == string::npos)
        // {
            DataItem d1, d2;
            d1.id = srcString;
            d2.id = dstString;
            Row r;
            r = r + d1;
            r = r + d2;
            r.updateQuery = "($ {id:\"" + srcString + "\"})-[:" + getSymbol() + "]->($ {id:\"" + dstString + "\"})";
            
            PathNode srcNode, dstNode, edgeNode;
            srcNode.id = srcString;
            srcNode.type = PathNode::NODE;
            
            dstNode.id = dstString;
            dstNode.type = PathNode::NODE;
            
            edgeNode.id = getSymbol();
            edgeNode.type = PathNode::EDGE_RIGHT;
            
            r.updatePath.push_back(srcNode);
            r.updatePath.push_back(edgeNode);
            r.updatePath.push_back(dstNode);
            
            r.prefixQuery = "($ {id:\"" + srcString + "\"})";
            r.suffixQuery = "($ {id:\"" + dstString + "\"})";
            r.dummySrc = srcString;
            r.dummyRelType = getSymbol();
            r.dummyDst = dstString;
            result.addRemoval(r);
        // }
        diff = neo4j_fetch_next(results);
    }
    
    Heading h1; h1.id = getSymbol(); h1.dirType = Heading::SRC;
    Heading h2; h2.id = getSymbol(); h2.dirType = Heading::DST;
    result.appendHeading(h1);
    result.appendHeading(h2);
    
    return result;
}


string JoinNode::getSymbol() const
{
    return "\u2A1D";
}

string UnionNode::getSymbol() const
{
    return "U";
}

string TransClosureNode::getSymbol() const
{
    return "+";
}

string ProjectionNode::getSymbol() const
{
    return "\u03C0";
}

string BaseFact::getSymbol() const
{
    return relName;
}

string InverseNode::getSymbol() const
{
    return "inv";
}

string BaseFact::getCypher() const
{
    return ":" + relName;
}

string InverseNode::getCypher() const
{
    return "<-[";
}

string JoinNode::getCypher() const
{
    return "]->($)";
}

string UnionNode::getCypher() const
{
    return "|";
}

string TransClosureNode::getCypher() const
{
    return "*";
}

string ProjectionNode::getCypher() const
{
    return "";
}


void BaseFact::handlePathNode(deque<PathNode> &path) const
{
    if(path.empty())
    {
        PathNode srcNode, dstNode, edge;
        srcNode.type = PathNode::NODE;
        dstNode.type = PathNode::NODE;
        edge.type = PathNode::EDGE_RIGHT;
        edge.id = relName;

        path.push_back(srcNode);
        path.push_back(edge);
        path.push_back(dstNode);
    }
    else
    {
        path[path.size() - 2].id = path[path.size() - 2].id + relName;
    }
    
}

void InverseNode::handlePathNode(deque<PathNode> &path) const
{
    std::reverse(path.begin(), path.end());
    for(int i = 0; i < path.size(); i++)
    {
        if(path[i].type == PathNode::EDGE_RIGHT)
        {
            path[i].type = PathNode::EDGE_LEFT;
        }
        else if(path[i].type == PathNode::EDGE_LEFT)
        {
            path[i].type = PathNode::EDGE_RIGHT;
        }
    }
}

void JoinNode::handlePathNode(deque<PathNode> &path) const
{
    PathNode edge, dstNode;
    edge.type = PathNode::EDGE_RIGHT;
    dstNode.type = PathNode::NODE;
    path.push_back(edge);
    path.push_back(dstNode);
}

void UnionNode::handlePathNode(deque<PathNode> &path) const 
{
    path[path.size() - 2].id = path[path.size() - 2].id + "|";
}

void ProjectionNode::handlePathNode(deque<PathNode> &path) const {}

void TransClosureNode::handlePathNode(deque<PathNode> &path) const
{
    path[path.size() - 2].maxLength = -1;
    path[path.size() - 2].id = path[path.size() - 2].id + "*";
}
