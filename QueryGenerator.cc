#include "QueryGenerator.h"

#include <boost/algorithm/string/replace.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>

using namespace std;

QueryGenerator::QueryGenerator(){}
QueryGenerator::~QueryGenerator(){}

string QueryGenerator::getRelFilter(const std::string &relExpr, const PathNode::Type &relDirection, bool reverse)
{
    vector<string> rels;
    string relFilter;
    boost::split(rels, relExpr, boost::is_any_of("|"));
    
    if(reverse)
    {
        if(relDirection == PathNode::EDGE_RIGHT)
        {
            relFilter = "<" + rels[0];
            for(int j = 1; j < rels.size(); j++)
            {
                relFilter = relFilter + "|<" + rels[j];
            }
        }
        else
        {
            relFilter = rels[0] + ">";
            for(int j = 1; j < rels.size(); j++)
            {
                relFilter = relFilter + "|" + rels[j] + ">";
            }
        }
    }
    else
    {
        if(relDirection == PathNode::EDGE_RIGHT)
        {
            relFilter = rels[0] + ">";
            for(int j = 1; j < rels.size(); j++)
            {
                relFilter = relFilter + "|" + rels[j] + ">";
            }
        }
        else
        {
            relFilter = "<" + rels[0];
            for(int j = 1; j < rels.size(); j++)
            {
                relFilter = relFilter + "|<" + rels[j];
            }
        }
    }
    return relFilter;
}

DeltaQueryGenerator::DeltaQueryGenerator() : QueryGenerator() {}
DeltaQueryGenerator::~DeltaQueryGenerator(){}

string DeltaQueryGenerator::generateQuery(const std::deque<PathNode> queryPath)
{
    string query;
    int prefixStart = 0, prefixNodes = 0, numNodes = 0;
    bool prefixFound = false;
    for(prefixStart; prefixStart < queryPath.size() && !prefixFound; prefixStart++)
    {
        prefixFound = queryPath[prefixStart].type == PathNode::NODE 
                                && !queryPath[prefixStart].id.empty();
        if(queryPath[prefixStart].type == PathNode::NODE)
        {
            prefixNodes++;
        }
    }
    preparePrefix(query, queryPath, prefixStart - 1, prefixNodes);
    prepareSuffix(query, queryPath, prefixStart, prefixNodes);
    return query;    
}

void DeltaQueryGenerator::preparePrefix(string &query, const deque<PathNode> &queryPath, int r, int numNodes)
{
    for(r; r >= 0; r--)
    {
        if(queryPath[r].type != PathNode::NODE)
        {
            string relFilter = getRelFilter(queryPath[r].id, queryPath[r].type, true); 
            if(queryPath[r].minLength == 0)
            {
                query = query + "call apoc.path.subgraphNodes(n" + to_string(numNodes) 
                                + ", {filterStartNode:false, labelFilter:'BaseFact', relationshipFilter:'" + relFilter +"'}) yield node as n" + to_string(numNodes - 1) + "\n";
            }
            else
            {
                int maxLevel = 1;
                if(queryPath[r].maxLength == -1)
                {
                    maxLevel = -1;
                    boost::erase_all(relFilter, "*");
                }
                
                query = query + "call apoc.path.subgraphNodes(n" + to_string(numNodes) 
                                + ", {maxLevel:" + to_string(maxLevel) + ",filterStartNode:false, labelFilter:'>BaseFact', relationshipFilter:'" + relFilter + "'}) yield node as n" + to_string(numNodes - 1) + "\n";
            }

            numNodes--;
        }
    }
}

void DeltaQueryGenerator::prepareSuffix(string &query, const deque<PathNode> &queryPath, int l, int numNodes)
{
    for(l; l < queryPath.size(); l++)
    {
        if(queryPath[l].type != PathNode::NODE)
        {
            string relFilter = getRelFilter(queryPath[l].id, queryPath[l].type, false);       
            if(queryPath[l].minLength == 0)
            {
                query = query + "call apoc.path.subgraphNodes(n" + to_string(numNodes) 
                                    + ", {filterStartNode:false, labelFilter:'BaseFact', relationshipFilter:'" + relFilter + "'}) yield node as n" + to_string(numNodes + 1) + "\n";
            }
            else
            {
                int maxLevel = 1;
                if(queryPath[l].maxLength == -1)
                {
                    maxLevel = -1;
                    boost::erase_all(relFilter, "*");
                }
                
                if(queryPath[l+1].id.empty())
                {
                    query = query + "call apoc.path.subgraphNodes(n" + to_string(numNodes) 
                                    + ", {maxLevel:" + to_string(maxLevel) + ",filterStartNode:false, labelFilter:'>BaseFact', relationshipFilter:'" + relFilter + "'}) yield node as n" + to_string(numNodes + 1) + "\n";
                }
                else
                {
                    query = query + "call apoc.path.subgraphNodes(n" + to_string(numNodes) 
                    + ", {maxLevel:" + to_string(maxLevel) + ",filterStartNode:false, labelFilter:'>BaseFact', relationshipFilter:'" + relFilter + "', terminatorNodes:n" + to_string(numNodes + 1) + "}) yield node\n";
                }
            }
            
        }
        else
        {
            numNodes++;
        } 
    } 
}

LocalisedQueryGenerator::LocalisedQueryGenerator(const vector<int> &proj) : QueryGenerator(), proj{proj} {}
LocalisedQueryGenerator::~LocalisedQueryGenerator(){}

string LocalisedQueryGenerator::generateQuery(const std::deque<PathNode> queryPath)
{
    string query;
    bool foundPrefix = false;
    int prefixStart = 0, prefixNodes = 0;
    for(prefixStart; prefixStart < queryPath.size() && !foundPrefix; prefixStart++)
    {
        foundPrefix = queryPath[prefixStart].type == PathNode::NODE && prefixNodes == proj[0];
        if(queryPath[prefixStart].type == PathNode::NODE)
        {
            prefixNodes++;
        }
    }
    preparePrefix(query, queryPath, prefixStart - 1, prefixNodes);
    prepareSuffix(query, queryPath, prefixStart, prefixNodes);
    return query;
}

void LocalisedQueryGenerator::preparePrefix(string &query, const deque<PathNode> &queryPath, int r, int numNodes)
{
    for(r; r >=0; r--)
    {
        if(queryPath[r].type != PathNode::NODE)
        {
            string relFilter = getRelFilter(queryPath[r].id, queryPath[r].type, true);
            if(queryPath[r].minLength == 0)
            {
                query = query + "call apoc.path.subgraphNodes(n" + to_string(numNodes) 
                                + ", {filterStartNode:false, labelFilter:'BaseFact', bfs:false, limit:1, relationshipFilter:'" + relFilter +"'}) yield node as n" + to_string(numNodes - 1) + "\n";
            }
            else
            {
                int maxLevel = 1;
                if(queryPath[r].maxLength == -1)
                {
                    maxLevel = -1;
                    boost::erase_all(relFilter, "*");
                }
                
                query = query + "call apoc.path.subgraphNodes(n" + to_string(numNodes) 
                                + ", {maxLevel:" + to_string(maxLevel) + ",filterStartNode:false, bfs:false, limit:1, labelFilter:'>BaseFact', relationshipFilter:'" + relFilter + "'}) yield node as n" + to_string(numNodes - 1) + "\n";   
            }
            numNodes--;
        }
    }
}

void LocalisedQueryGenerator::prepareSuffix(string &query, const deque<PathNode> &queryPath, int l, int numNodes)
{
    int tmps = 0;
    int projIndex = 1;
    for(l; l < queryPath.size(); l++)
    {
        if(queryPath[l].type != PathNode::NODE)
        {
            string relFilter = getRelFilter(queryPath[l].id, queryPath[l].type, false);
            int maxLevel = 1;
            if(queryPath[l].maxLength == -1)
            {
                maxLevel = -1;
                boost::erase_all(relFilter, "*");
            }
            
            if(projIndex < proj.size() && numNodes == proj[projIndex])
            {
                query = query + "call apoc.path.subgraphNodes(n" + to_string(numNodes) 
                + ", {maxLevel:" + to_string(maxLevel) + ",filterStartNode:false, bfs:false, limit:1, labelFilter:'>BaseFact', relationshipFilter:'" + relFilter + "', terminatorNodes:n" + to_string(numNodes + 1) + "}) yield node as tmps" + to_string(tmps) + "\n";
                tmps++;
                projIndex++;
            }
            else
            {
                query = query + "call apoc.path.subgraphNodes(n" + to_string(numNodes) 
                                + ", {maxLevel:" + to_string(maxLevel) + ",filterStartNode:false, bfs:false, limit:1, labelFilter:'>BaseFact', relationshipFilter:'" + relFilter + "'}) yield node as n" + to_string(numNodes + 1) + "\n";
            }
        }
        else
        {
            numNodes++;
        }
    }
}