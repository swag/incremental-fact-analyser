 
#pragma once 
 
#include <list>
#include <deque>
#include <vector>
#include <string>
#include <iostream>


struct DataItem
{
    
    enum TransType
    {
      NONE, 
      SRC, //+id
      DST  //id+
    };
    
    std::string id;
    TransType transType;
    
    DataItem()
    {
        transType = TransType::NONE;
    }
    
    friend bool operator==(const DataItem &lhs, const DataItem &rhs)
    {
        return lhs.id == rhs.id;
    }
    
    friend bool operator<(const DataItem &lhs, const DataItem &rhs)
    {
        return lhs.id < rhs.id;
    }
};

struct PathNode
{
    enum Type
    {
        NODE,
        EDGE_RIGHT,
        EDGE_LEFT
    };
    
    
    std::string id;
    int minLength = 1;
    int maxLength = 1;
    Type type;
};

struct Row
{
    std::deque<DataItem> columns;
    typedef std::deque<DataItem>::iterator row_iterator;
    typedef std::deque<DataItem>::const_iterator const_row_iterator;
    
    void appendColumn(const DataItem &newCol);
    void prependColumn(const DataItem &newCol);
    void replaceColumn(int col, const DataItem &newCol);
    
    void reverse();
    
    const DataItem& getCol(int index) const;
    DataItem& getCol(int index);
    
    row_iterator begin();
    const_row_iterator begin() const;
    
    row_iterator end();
    const_row_iterator end() const;
    
    void operator=(const Row &other)
    {
        columns = other.columns;
    }
    
    void copyPattern(const Row &other)
    {
        columns = other.columns;
    }
    
    int size() const
    {
        return columns.size();
    }
    
    bool empty() const
    {
        return size() == 0;
    }
    
    Row getRange(int startIndex, int endIndex) const;
    
    const DataItem& operator[](int index) const
    {
        return getCol(index);
    }
    
    DataItem& operator[](int index)
    {
        return getCol(index);
    }
    
    Row getPrefix(int endIndex) const;
    Row getSuffix(int startIndex) const;
    
    friend bool operator==(const Row &lhs, const Row &rhs)
    {
        return lhs.columns == rhs.columns;
    }
    
    friend bool operator<(const Row &lhs, const Row &rhs)
    {
        return lhs.columns < rhs.columns;
    }
    
    friend Row operator+(Row &lhs, const Row &rhs)
    {
        lhs.columns.insert(lhs.end(), rhs.begin(), rhs.end());
        return lhs;
    }
    
    friend Row operator+(Row &lhs, const DataItem &rhs)
    {
        lhs.appendColumn(rhs);
        return lhs;
    }
    
    
    
    int getPrefixStart() const;
    int getSuffixStart() const;
    
    // Do Update Query Generation Here
    // Can't think of better abstraction, so let's
    // keep it simple for now
    std::string updateQuery;
    
    
    std::deque<PathNode> updatePath;
    
    std::string prefixQuery;
    std::string suffixQuery;
    // For Removals
    // Should refactor to something more elegant
    std::string dummySrc;
    std::string dummyDst;
    std::string dummyRelType;
    
    friend std::ostream& operator<<(std::ostream &out, const Row &row);
};

namespace std
{
    template <>
    struct hash<Row>
    {
        std::size_t operator()(const Row &r) const
        {
            std::string tmp;
            for(int i = 0; i < r.size(); i++)
            {
                tmp = tmp + r[i].id;
            }
            return std::hash<std::string>()(tmp); 
        }
    };
    
}

struct PatternTable
{
    std::vector<Row> rows;
    typedef std::vector<Row>::iterator table_iterator;
    typedef std::vector<Row>::const_iterator const_table_iterator;
    
    void addRow(const Row &row);
    
    bool empty() const
    {
        return rows.empty();
    }
    
    Row& operator[](int index)
    {
        return rows[index];
    }
    
    int size() const
    {
        return rows.size();
    }
    
    
    table_iterator begin();
    const_table_iterator begin() const;
    
    table_iterator end();
    const_table_iterator end() const;
};


struct Heading
{
    enum Direction
    {
        SRC,
        DST
    };
    
    std::string id;
    Direction dirType;
};

class Change
{
    public:
        Change();
        ~Change();
        
        //~ void operator= (const Change &other);
        void join(Change &other);
        void _union(const Change &other);
        void addAll(const Change &other);
        
        void prependToPatterns(const DataItem &str);
        void prependHeading(const Heading &str);
        void appendToPatterns(const DataItem &str);
        void appendHeading(const Heading &str);
        void setHeading(int index, const Heading &str);
        
        void addAddition(const Row &row);
        void addRemoval(const Row &row);
        
        void replaceColumn(int col, const DataItem &data);
        
        void reverseHeading();
        
        bool hasAdditionPatterns();
        
        PatternTable& getAdditionPatterns();
        PatternTable& getRemovalPatterns();

        const PatternTable& getAdditionPatterns() const;
        const PatternTable& getRemovalPatterns() const;
        
        Row& getAdditionPattern(int index);
        Row& getRemovalPattern(int index);
        
        int getNumHeadings();
        Heading getHeading(int index);
        
        friend std::ostream& operator<<(std::ostream &out, const Change &change);
        
    private:
        // These contain 'dirty' changes
        PatternTable additionPatterns;
        // These contain outright removals
        PatternTable removalPatterns;
        std::deque<Heading> headings;
        
};
