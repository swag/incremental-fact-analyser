
#include <string>
#include <vector>
#include <deque>
#include <neo4j-client.h>

class Change;
struct PathNode;

class DependencyNode
{
    public:
        DependencyNode(std::string aLabel);
        DependencyNode(std::string aLabel, std::vector<int> cols);
        virtual ~DependencyNode();
        virtual Change propogateChange(neo4j_connection_t *connection) const = 0;
        virtual std::string getSymbol() const = 0;
        virtual std::string getCypher() const = 0;
        virtual void handlePathNode(std::deque<PathNode> &path) const = 0;
        virtual std::string getLabel();
        
        void setLeftChild(DependencyNode *left);
        void setRightChild(DependencyNode *right);
        const std::vector<int>& getProjectedCols() const
        {
            return cols;
        }
        
        DependencyNode* getLeftChild() const;
        DependencyNode* getRightChild() const;
    
    protected:
        DependencyNode *leftChild;
        DependencyNode *rightChild;
        std::string label;
        std::vector<int> cols;
};

class JoinNode : public DependencyNode
{
    public:
        JoinNode(std::string aLabel);
    
        virtual Change propogateChange(neo4j_connection_t *connection) const override;
        virtual std::string getSymbol() const override;
        virtual std::string getCypher() const override;
        virtual void handlePathNode(std::deque<PathNode> &path) const override;

};

class UnionNode : public DependencyNode
{
    public:
        UnionNode(std::string aLabel);
    
        virtual Change propogateChange(neo4j_connection_t *connection) const override;
        virtual std::string getSymbol() const override;
        virtual std::string getCypher() const override;
        virtual void handlePathNode(std::deque<PathNode> &path) const override;
};

class TransClosureNode : public DependencyNode
{
    
    public:
        TransClosureNode(std::string aLabel);
    
        virtual Change propogateChange(neo4j_connection_t *connection) const override;
        virtual std::string getSymbol() const override;
        virtual std::string getCypher() const override;
        virtual void handlePathNode(std::deque<PathNode> &path) const override;
};

class ProjectionNode : public DependencyNode
{
    public:
        ProjectionNode(std::string aLabel, std::vector<int> cols);
        
        virtual Change propogateChange(neo4j_connection_t *connection) const override;
        virtual std::string getSymbol() const override;
        virtual std::string getCypher() const override;
        virtual void handlePathNode(std::deque<PathNode> &path) const override;
        
};

// TODO
class InverseNode : public DependencyNode
{
    public:
        InverseNode(std::string aLabel);
  
        virtual Change propogateChange(neo4j_connection_t *connection) const override;
        virtual std::string getSymbol() const override;
        virtual std::string getCypher() const override;
        virtual void handlePathNode(std::deque<PathNode> &path) const override;
};


class BaseFact : public DependencyNode
{
    public:
        BaseFact(std::string relName);
    
        virtual Change propogateChange(neo4j_connection_t *connection) const override;
        virtual std::string getSymbol() const override;
        virtual std::string getCypher() const override;
        virtual void handlePathNode(std::deque<PathNode> &path) const override;
        
    private:
        std::string relName;
    
};
