// Batch Query for call o write o varWrite

call apoc.periodic.iterate(
  "MATCH (n1:BaseFact)\n
  call apoc.path.subgraphNodes(n1, {maxLevel:1, filterStartNode:false, labelFilter:'>BaseFact', relationshipFilter:'call>'}) yield node as n2\n
  call apoc.path.subgraphNodes(n2, {maxLevel:1, filterStartNode:false, sequence:'>BaseFact, write>'}) yield node as n3\n
  call apoc.path.subgraphNodes(n3, {maxLevel:1, filterStartNode:false, sequence:'>BaseFact, varWrite>'}) yield node as n4\n
  return n1, n2, n3, n4",
  "Merge (m:DerivedFact {n1:n1.id, n2:n2.id, n3:n3.id, n4:n4.id})", {batchSize:1000, iterateList:true}
)
