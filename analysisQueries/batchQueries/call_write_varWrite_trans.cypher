// Batch Query for call o (write + varWrite)+

call apoc.periodic.iterate(
  "MATCH (n1:BaseFact)\n
  call apoc.path.subgraphNodes(n1, {maxLevel:1, filterStartNode:false, labelFilter:'>BaseFact', relationshipFilter:'call>'}) yield node as n2\n
  call apoc.path.subgraphNodes(n2, {filterStartNode:false, sequence:'>BaseFact, write>|varWrite>'}) yield node as n3\n
  return n1, n2, n3",
  "Merge (m:DerivedFact {n1:n1.id, n2:n2.id, n3:n3.id})", {batchSize:1000, iterateList:true}
)
