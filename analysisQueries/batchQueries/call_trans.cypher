// Batch Query for call+

call apoc.periodic.iterate(
  "MATCH (n1:BaseFact)\n
  call apoc.path.subgraphNodes(n1, {filterStartNode:false, labelFilter:'>BaseFact', relationshipFilter:'call>'}) yield node as n2\n
  return n1, n2",
  "Merge (m:DerivedFact {n1:n1.id, n2:n2.id})", {batchSize:1000, iterateList:true}
)
