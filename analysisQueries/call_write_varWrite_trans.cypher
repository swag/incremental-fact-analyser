// call o (write + varWrite)+

match (n1)-[:call]->(n2)-[:write|varWrite*]->(n3)
return n1.id, n2.id, n3.id
