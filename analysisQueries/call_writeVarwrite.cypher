// call o write o varWrite

match (n1)-[:call]->(n2)-[:write]->(n3)-[:varWrite]->(n4)
return n1.id, n2.id, n3.id, n4.id
