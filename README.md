# Incremental Fact Analysis Prototype

This repository contains the proportype for an incremental fact analyzer, using Neo4j 3.5.6 as its backend.

## Dependencies

To *build* the prototype, you first need to install the following dependencies.

### CMake

Make sure the version of CMake that you're using is at least `3.3`. You can install the latest version maintained on your OS' package manager. For Debian-based systems, e.g. Ubuntu, you can install CMake by running the following command:
```
$ sudo apt-get install cmake
```
To check the version of CMake installed, run:
```
$ cmake -version
```

### Boost Libraries

Boost version `1.60` or higher is required. You can install the latest version maintained on your OS' package manager. For Debian-based systems, e.g. Ubuntu, you can install the libraries by running the following command:
```
$ sudo apt-get install libboost-all-dev
```
If this version of Boost is not sufficient, then you will have to either install or build a version from source. For the latter, make sure the following environment variable is set:
```
$ export BOOST_ROOT=<path_to_boost_build>
```

### Neo4j Server (Community Edition)
Neo4j version `3.5.X` is required. You can either install it using your OS' package manager, or install it from source. More information can be found here: https://neo4j.com/docs/operations-manual/current/installation/

Once you've installed Neo4j, install the `APOC 3.5.0.4` plugin. The easiest way to do this is to download the JAR file
from https://github.com/neo4j-contrib/neo4j-apoc-procedures/releases into the `plugins` directory. After that, make the following changes to the Neo4j config file `neo4j.conf`:

* Comment out `dbms.directories.import`. This will allow Neo4j to load CSV files from any directory
* Set `dbms.tx_log.rotation.retention_policy` to `false`. This tells Neo4j to discard Transaction histories, freeing up disk space. Do this only when disk space becomes an issue

### Neo4j Client Library
Refer to these instructions to install this dependency:https://neo4j-client.net/


### Cypher Parser Library
Refer to these instructions to install this dependency: http://cleishm.github.io/libcypher-parser/

## Build
Once all dependencies have been installed, the prototype can built using these commands:
```
$ cmake -G "Unix Makefiles" -DNEO4J_CLIENT=<path_to_install_directory> -DCYPHER_PARSER-<path_to_install_directory>
$ make
```
Note that the CMake variables `NEO4J_CLIENT` and `CYPHER_PARSER` are optional