#pragma once

#include <string>
#include <vector>
#include <deque>
#include <set>
#include <unordered_set>
#include <thread>
#include <mutex>
#include <iostream>
#include <neo4j-client.h>

class Row;
class Change;
struct PatternTable;
struct PathNode;
class ResultSet;
class DependencyNode;

class ChangeHandler
{
    public:
        ChangeHandler(const DependencyNode* root, std::ostream &log = std::cout);
        void handleChange(const Change &change, neo4j_connection_t *connection, const std::string &neo4j_dbURL, const std::vector<int> &proj);
        
    protected:
        void createDummyChange(const PatternTable &removals, neo4j_connection_t *connection);
        void cleanUpDummies(neo4j_connection_t *connection);
        void handleRemovals(const PatternTable &removals, neo4j_connection_t *connection, 
                const std::string &neo4j_dbURL, const std::vector<int> &proj, const std::string &derivedFact_id);
        void handleAdditions(const PatternTable &adds, neo4j_connection_t *connection, 
                const std::string &neo4j_dbURL, const std::vector<int> &proj, const std::string &derivedFact_id);
        template<typename iterator_type> void process_dQueries(iterator_type start, iterator_type end, 
            int numWorkers, const std::string &neo4j_dbURL, std::function<void(neo4j_connection_t*, const Row &dQuery)> job);

    private:
        void executeDeltaQuery(const Row &currChange, const std::vector<int> &proj, std::unordered_set<Row> &result, std::mutex &m, neo4j_connection_t *connection);
        bool reInsert(const Row &r, const std::vector<int> &proj, neo4j_connection_t *connection);
                
        std::string preparedQuery;
        std::deque<PathNode> preparedPath;
        std::ostream &log;
};

template<typename iterator_type> void ChangeHandler::process_dQueries(iterator_type start, iterator_type end, int numWorkers, 
        const std::string &neo4j_dbURL, std::function<void(neo4j_connection_t*, const Row &dQuery)> job)
{
    std::vector<std::thread> workers;
    std::mutex job_mutex;
    for(int i = 0; i < numWorkers; i++)
    {
        workers.push_back(std::thread([&start, &end, &job_mutex, &neo4j_dbURL, job]()
        {
            neo4j_client_init();
            neo4j_connection_t *workerConnection =
                    neo4j_connect(neo4j_dbURL.c_str(), NULL, NEO4J_INSECURE);
            if (workerConnection == NULL)
            {
                std::cout << "Failed to establish connection!" << std::endl;
                exit(EXIT_FAILURE);
            }
            while (true) 
            {
                job_mutex.lock();
                if(start == end) 
                {
                    job_mutex.unlock();
                    break;
                }
                auto currentJob_it = start;
                start++;
                job_mutex.unlock();
                job(workerConnection, *currentJob_it);
            }
            neo4j_close(workerConnection);
            neo4j_client_cleanup();

        }));
    }
    for(std::thread &worker : workers){
        worker.join();
    }
}
