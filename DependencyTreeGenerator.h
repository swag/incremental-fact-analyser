#pragma once

#include <string>
#include <vector>
#include <set>
#include <deque>
#include <unordered_map>
#include <cypher-parser.h>

class DependencyNode;
class Change;
class Row;

class DependencyTreeGenerator
{
    public:
         DependencyNode* generateTree(std::string query);
         DependencyNode* generateTreeFromFile(std::string filename);
        
    protected:
         DependencyNode* recursiveConstruct(int elementIndex, const cypher_astnode_t *patternPath);
         DependencyNode* createRelationPatternSubTree(const cypher_astnode_t *relPattern);
         DependencyNode* constructUnion(const cypher_astnode_t *relPattern, unsigned int numRels);
         std::unordered_map<std::string, int> getColumnMapping(const cypher_astnode_t *path);
         bool generateTreeFromQuery(std::vector<DependencyNode*> &matchTrees, 
                std::vector<std::string> &projections, std::deque<std::string> &resultSchema, const cypher_astnode_t *query);
         void parseMATCH(std::vector<DependencyNode*> &matchTrees, 
                            std::vector<std::string> &projections, std::deque<std::string> &resultSchema, const cypher_astnode_t *currClause);
         void parseWITH(std::vector<DependencyNode*> &matchTrees, std::vector<std::string> &projections, const cypher_astnode_t *currClause);
         void parseRETURN(std::vector<DependencyNode*> &matchTrees, 
                    std::vector<std::string> &projections, std::deque<std::string> &resultSchema, const cypher_astnode_t *currClause);
};
