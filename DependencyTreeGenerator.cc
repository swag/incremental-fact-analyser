#include "DependencyTreeGenerator.h"
#include "DependencyNode.h"

#include "Change.h"
#include <iostream>
#include <unordered_map>
#include <map>
#include <deque>
#include <algorithm>
#include <stdio.h>

using namespace std;
DependencyNode* DependencyTreeGenerator::generateTreeFromFile(std::string filename)
{
    FILE* file = fopen(filename.c_str(), "r");
    cypher_parse_result_t *result = cypher_fparse(
            file, NULL, NULL, CYPHER_PARSE_ONLY_STATEMENTS);
            
    if(result == NULL)
    {
        throw "Unable to Read File";
    }
    unsigned int numStatements = cypher_parse_result_ndirectives(result);
    vector<DependencyNode*> matchTrees;
    vector<string> projections;
    deque<string> resultSchema;
    bool finished = false;
    for(unsigned int currStatement = 0; currStatement < numStatements && !finished; currStatement++)
    {
        const cypher_astnode_t *ast = cypher_parse_result_get_directive(result, currStatement);
        const cypher_astnode_t *query = cypher_ast_statement_get_body(ast);
        finished = generateTreeFromQuery(matchTrees, projections, resultSchema, query);
    }
    
    cypher_parse_result_free(result);
    return matchTrees.back();
    
}

DependencyNode* DependencyTreeGenerator::generateTree(std::string queryStr)
{
    cypher_parse_result_t *result = cypher_parse(
            queryStr.c_str()
            , NULL, NULL, CYPHER_PARSE_ONLY_STATEMENTS);
            
    if (result == NULL)
    {
        perror("cypher_parse");
        return nullptr;
    }

    const cypher_astnode_t *ast = cypher_parse_result_get_directive(result, 0);
    const cypher_astnode_t *query = cypher_ast_statement_get_body(ast);
    const cypher_astnode_t *match = cypher_ast_query_get_clause(query, 0);

    const cypher_astnode_t *pattern = cypher_ast_match_get_pattern(match);
    const cypher_astnode_t *path = cypher_ast_pattern_get_path(pattern, 0);
    const cypher_astnode_t *returnNode = cypher_ast_query_get_clause(query, 1);
    
    unsigned int numPathElements = cypher_ast_pattern_path_nelements(path);
    int elementIndex = numPathElements - 2;
    DependencyNode *root = recursiveConstruct(elementIndex, path);
    
    cypher_parse_result_free(result);
    return root;
}

DependencyNode* DependencyTreeGenerator::recursiveConstruct(int elementIndex, const cypher_astnode_t *patternPath)
{
    if(elementIndex > 1)
    {
        DependencyNode *rightChild = createRelationPatternSubTree(cypher_ast_pattern_path_get_element(patternPath, elementIndex));
        DependencyNode *leftChild = recursiveConstruct(elementIndex - 2, patternPath);
        
        DependencyNode *subTree = new JoinNode(leftChild->getLabel());
        subTree->setLeftChild(leftChild);
        subTree->setRightChild(rightChild);
        return subTree;
    }
    else
    {
        return createRelationPatternSubTree(cypher_ast_pattern_path_get_element(patternPath, elementIndex));
    }
}

DependencyNode* DependencyTreeGenerator::createRelationPatternSubTree(const cypher_astnode_t *relPattern)
{
    DependencyNode *currNode = nullptr;
    
    // Assume direction is outgoing for now...
    enum cypher_rel_direction direction = cypher_ast_rel_pattern_get_direction(relPattern);
    const cypher_astnode_t *varLength = cypher_ast_rel_pattern_get_varlength(relPattern);
    const cypher_astnode_t *reltype = cypher_ast_rel_pattern_get_reltype(relPattern, 0);
    unsigned int numRels = cypher_ast_rel_pattern_nreltypes(relPattern);
    //~ cout << "RelType: " << cypher_ast_reltype_get_name(reltype) << endl; 
    if(varLength != NULL)
    {
        const cypher_astnode_t *startLength = cypher_ast_range_get_start(varLength);
        const cypher_astnode_t *endLength = cypher_ast_range_get_end(varLength);
        
        if(startLength == NULL && endLength == NULL)
        {
            //TransClosure
            
            DependencyNode *leftChild = constructUnion(relPattern, numRels);
            currNode = new TransClosureNode(leftChild->getLabel());
            currNode->setLeftChild(leftChild);
            
            if(direction == 0)
            {
                DependencyNode *inverseNode = new InverseNode(currNode->getLabel());
                inverseNode->setRightChild(currNode);
                return inverseNode;
            }
            
            return currNode;
        }
        
    }
    DependencyNode *unionNode = constructUnion(relPattern, numRels);
    if(direction == 0)
    {
        DependencyNode *inverseNode = new InverseNode(unionNode->getLabel());
        inverseNode->setRightChild(unionNode);
        return inverseNode;
    }
    return unionNode;
}

DependencyNode* DependencyTreeGenerator::constructUnion(const cypher_astnode_t *relPattern, unsigned int numRels)
{

    DependencyNode *root = nullptr;
    string relName = cypher_ast_reltype_get_name(cypher_ast_rel_pattern_get_reltype(relPattern, numRels - 1));
    if(numRels > 1)
    {
        DependencyNode *rightChild = new BaseFact(relName);
        DependencyNode *leftChild = constructUnion(relPattern, numRels - 1);
        
        root = new UnionNode(leftChild->getLabel() + " U " + rightChild->getLabel());
        root->setLeftChild(leftChild);
        root->setRightChild(rightChild);
        return root; 
    }
    else
    {
        //~ currNode->data = cypher_ast_reltype_get_name(reltype);
        return new BaseFact(relName);
    }
}


unordered_map<string, int> DependencyTreeGenerator::getColumnMapping(const cypher_astnode_t *path)
{
    unordered_map<string, int> colMappings;
    int currCol = 0;
    unsigned int numPathElements = cypher_ast_pattern_path_nelements(path);
    for(int i = 0; i < numPathElements; i += 2)
    {
        const cypher_astnode_t *nodeID = cypher_ast_node_pattern_get_identifier(cypher_ast_pattern_path_get_element(path, i));
        if(nodeID)
        {
            string nodeIdentifier = cypher_ast_identifier_get_name(nodeID);
            colMappings[nodeIdentifier] = currCol;
            colMappings["max"] = currCol;
        }
        currCol++;
    }
    return colMappings;
}

bool DependencyTreeGenerator::generateTreeFromQuery(vector<DependencyNode*> &matchTrees, 
        vector<string> &projections, deque<string> &resultSchema, const cypher_astnode_t *query)
{
    unsigned int numClauses = cypher_ast_query_nclauses(query);
    for(unsigned int i = 0; i < numClauses; i++)
    {
        const cypher_astnode_t *currClause = cypher_ast_query_get_clause(query, i);
        if(cypher_astnode_instanceof(currClause, CYPHER_AST_MATCH))
        {
            parseMATCH(matchTrees, projections, resultSchema, currClause);
        }
        else if(cypher_astnode_instanceof(currClause, CYPHER_AST_WITH))
        {
            parseWITH(matchTrees, projections, currClause);
        }
        else if(cypher_astnode_instanceof(currClause, CYPHER_AST_RETURN))
        {
            parseRETURN(matchTrees, projections, resultSchema, currClause);
            return true;
        }
    }
    
    return false;
    
}


void DependencyTreeGenerator::parseMATCH(vector<DependencyNode*> &matchTrees, 
    vector<string> &projections, deque<string> &resultSchema, const cypher_astnode_t *currClause)
{
    const cypher_astnode_t *pattern = cypher_ast_match_get_pattern(currClause);
    const cypher_astnode_t *path = cypher_ast_pattern_get_path(pattern, 0);
    unsigned int numPathElements = cypher_ast_pattern_path_nelements(path);
    int elementIndex = numPathElements - 2;
    DependencyNode *root = recursiveConstruct(elementIndex, path);
    
    if(projections.empty())
    {
        matchTrees.push_back(root);
        for(int i = 0; i < numPathElements; i += 2)
        {
            const cypher_astnode_t *nodeID = cypher_ast_node_pattern_get_identifier(cypher_ast_pattern_path_get_element(path, i));
            if(nodeID)
            {
                string nodeIdentifier = cypher_ast_identifier_get_name(nodeID);
                resultSchema.push_back(nodeIdentifier);
            }
        }
    }
    else
    {
        DependencyNode *prev = matchTrees.back();
        matchTrees.pop_back();
        unordered_map<string, int> cols = getColumnMapping(path);
        if(cols.find(projections.front()) != cols.end() && cols[projections.front()] == cols["max"])
        {
            cout << "Right Join!" << endl;
            DependencyNode *newRoot = new JoinNode(root->getLabel());
            newRoot->setLeftChild(root);
            newRoot->setRightChild(prev);
            matchTrees.push_back(newRoot);
            map<int, string> orderedCol;
            for(auto &it : cols)
            {
                if(it.first != projections.front() && it.first != "max")
                {
                    orderedCol[it.second] = it.first;
                }
            }
            
            for(auto &it : orderedCol)
            {
                resultSchema.push_back(it.second);
            }
            
        }
        else if(cols.find(projections.back()) != cols.end() && cols[projections.back()] == 0)
        {
            cout << "Left Join" << endl;
            DependencyNode *newRoot = new JoinNode(prev->getLabel());
            newRoot->setLeftChild(prev);
            newRoot->setRightChild(root);
            matchTrees.push_back(newRoot);
            map<int, string> orderedCol;
            for(auto &it : cols)
            {
                if(it.first != projections.back() && it.first != "max")
                {
                    orderedCol[it.second] = it.first;
                }
            }
            
            for(auto &it : orderedCol)
            {
                resultSchema.push_back(it.second);
            }
        }
        else
        {
            throw "Unable to Generate Dependency Tree!";
        }
    }
}

void DependencyTreeGenerator::parseWITH(vector<DependencyNode*> &matchTrees, vector<string> &projections, const cypher_astnode_t *currClause)
{
    projections.clear();
    unsigned int numProjections =  cypher_ast_with_nprojections(currClause);
    for(unsigned int i = 0; i < numProjections; i++)
    {
        const cypher_astnode_t *projection = cypher_ast_with_get_projection(currClause, i);
        string nodeID = cypher_ast_identifier_get_name(cypher_ast_projection_get_expression(projection));
        const cypher_astnode_t *alias = cypher_ast_projection_get_alias(projection);
        if(alias)
        {
            cout << "Alias: " << cypher_ast_identifier_get_name(alias) << endl;
            projections.push_back(cypher_ast_identifier_get_name(alias));
        }
        else
        {
            cout << "Projection: " << nodeID << endl;
            projections.push_back(nodeID);
        }
    }
}

void DependencyTreeGenerator::parseRETURN(vector<DependencyNode*> &matchTrees, 
        vector<string> &projections, deque<string> &resultSchema, const cypher_astnode_t *currClause)
{
    DependencyNode *finalTree = matchTrees.back();
    matchTrees.pop_back();
    vector<int> projectedCols;
    unsigned int numProjections =  cypher_ast_return_nprojections(currClause);
    for(unsigned int i = 0; i < numProjections; i++)
    {
        const cypher_astnode_t *projection = cypher_ast_return_get_projection(currClause, i);
        const cypher_astnode_t *exp = cypher_ast_projection_get_expression(projection);
        string nodeID;
        if(cypher_astnode_instanceof(exp, CYPHER_AST_PROPERTY_OPERATOR))
        {
            nodeID = cypher_ast_identifier_get_name(cypher_ast_property_operator_get_expression(exp));
        }
        else
        {
            nodeID = cypher_ast_identifier_get_name(exp);
        }
        //~ const cypher_astnode_t *alias = cypher_ast_projection_get_alias(projection);
        auto it = find(resultSchema.begin(), resultSchema.end(), nodeID);
        if(it != resultSchema.end())
        {
            projectedCols.push_back(distance(resultSchema.begin(), it));
        }
    }
    DependencyNode *projection = new ProjectionNode("p", projectedCols);
    projection->setRightChild(finalTree);
    matchTrees.push_back(projection);
}


