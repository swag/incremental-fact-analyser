#include <cypher-parser.h>
#include <errno.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <neo4j-client.h>

#include "DependencyTreeGenerator.h"
#include "DependencyNode.h"
#include "Change.h"
#include "ChangeHandler.h"

#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <chrono> 

#include <set>
#include <cassert>
#include <fstream>
#include <sstream>
using namespace std;
using namespace std::chrono;
namespace fs = boost::filesystem;
// Result Set operations
ResultSet readResultSet(string fname);

void printInOrder(DependencyNode* root, string& result);
void getInOrder(DependencyNode* root, vector<DependencyNode*> &result);

int main(int argc, char *argv[])
{
    
    if(argc < 5)
    {
        cerr << "Usage: cypherParse inputScript batchScript neo4j_username neo4j_password logFile" << endl;
        return 1;
    }
    
    ostream *log;
    if(argc > 5)
    {
        log = new fs::ofstream(fs::absolute(fs::path{argv[5]}), ios_base::app);
    }
    else
    {
        log = &cout;
    }
    fs::path inputScript = fs::absolute(fs::path{argv[1]});
    fs::path batchScript = fs::absolute(fs::path{argv[2]});
    string neo4j_username(argv[3]);
    string neo4j_password(argv[4]);
    string neo4j_dbURL = "neo4j://" + neo4j_username + ":" + neo4j_password + "@localhost:7687";

    DependencyTreeGenerator depTreeGen;
    DependencyNode* rootFromFile = depTreeGen.generateTreeFromFile(inputScript.string());
    int numCols_DerivedFact = rootFromFile->getProjectedCols().size();
    string query_downloadResults = "Match (m:DerivedFact) Return m.n1";
    for(int i = 1; i < numCols_DerivedFact; i++)
    {
        query_downloadResults = query_downloadResults + ", m.n" + to_string(i+1);
    }
    cerr << query_downloadResults << endl;

    neo4j_client_init();
    /* use NEO4J_INSECURE when connecting to disable TLS */
    neo4j_connection_t *connection =
            neo4j_connect(neo4j_dbURL.c_str(), NULL, NEO4J_INSECURE);
    if (connection == NULL)
    {
        neo4j_perror(stderr, errno, "Connection failed");
        return EXIT_FAILURE;
    }
    
    string inOrderTree;
    printInOrder(rootFromFile, inOrderTree);
    cout << inOrderTree << endl;
    high_resolution_clock::time_point beginTime, endTime;
    duration<double> incrementalTime, batchTime, propogationTime;
    ChangeHandler ch(rootFromFile, *log);
    beginTime = high_resolution_clock::now();
    Change finalChange = rootFromFile->propogateChange(connection);
    propogationTime = duration_cast<duration<double>>(high_resolution_clock::now() - beginTime);
    *log << "Propogation Time(s)," << propogationTime.count() << endl;
    
    beginTime = high_resolution_clock::now();
    ch.handleChange(finalChange, connection, neo4j_dbURL, rootFromFile->getProjectedCols());
    endTime = high_resolution_clock::now();
    incrementalTime = duration_cast<duration<double>>(endTime - beginTime);
    
    
    cerr << "Downloading Actual Results..." << endl;
    FILE *actualResult = fopen("actual.csv", "w");
    neo4j_render_csv(actualResult, neo4j_run(connection, query_downloadResults.c_str(), neo4j_null), NEO4J_RENDER_DEFAULT);
    
    stringstream batchQuery_buffer;
    batchQuery_buffer << fs::ifstream{batchScript}.rdbuf();
    cerr << "Performing Batch Analysis..." << endl;
    beginTime = high_resolution_clock::now();
    neo4j_close_results(neo4j_send(connection, "Call apoc.periodic.iterate('Match (n:DerivedFact) Return n', 'Delete n', {batchSize:1000, iterateList:true})", neo4j_null));
    neo4j_close_results(neo4j_send(connection, batchQuery_buffer.str().c_str(), neo4j_null));
    batchTime = duration_cast<duration<double>>(high_resolution_clock::now() - beginTime); 
    
    cerr << "Downloading Expected Results..." << endl;
    FILE *expectedResult = fopen("expected.csv", "w");
    neo4j_render_csv(expectedResult, neo4j_run(connection, query_downloadResults.c_str(), neo4j_null), NEO4J_RENDER_DEFAULT);
    
    
    neo4j_close(connection);
    neo4j_client_cleanup();
    
    
    *log << "Analysis Time (s)," << incrementalTime.count() << endl;
    *log << "Incremental Time (s)," << incrementalTime.count() + propogationTime.count() << endl;
    *log << "Batch Time (s)," << batchTime.count() << endl;
    return EXIT_SUCCESS;
    
}

void printInOrder(DependencyNode* root, string& result)
{
    if(root)
    {
        printInOrder(root->getLeftChild(), result);
        result = result + root->getSymbol() + " ";
        printInOrder(root->getRightChild(), result);
    }
    
}
