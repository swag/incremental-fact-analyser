#!/bin/bash
maxLimit=$1
for i in `seq 1 1 $maxLimit`
do
    echo "Checking Results with Change Limit $i..."
    sort actual_factBase_$i.csv > actual_factBase_$i.csv.sorted
    sort actual_resultSet_$i.csv > actual_resultSet_$i.csv.sorted
    sort expected_$i.csv > expected_$i.csv.sorted
    
    pass=`comm -23 actual_factBase_$i.csv.sorted expected_$i.csv.sorted | wc -l`
    if [ "$pass" != "0" ] 
    then
        echo "actual_$i.csv.sorted and expected_$i.csv.sorted differ!"
        exit 0
    fi
    
    numExtraFacts=`comm -23 actual_resultSet_$i.csv.sorted expected_$i.csv.sorted | wc -l`
    numMissing=`comm -13 actual_resultSet_$i.csv.sorted expected_$i.csv.sorted | wc -l`
    
    echo "Number of Extra Facts using Result Set: $numExtraFacts"
    echo "Number of Missing Facts using Result Set: $numMissing"
    
    echo "Pass"
    
done

echo "All Tests Passed"
