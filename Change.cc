#include "Change.h"

#include <string>
#include <iostream>
#include <algorithm>

using namespace std;
Change::Change(){}

Change::~Change()
{}

//~ void Change::operator= (const Change &other)
//~ {}

void Change::addAll(const Change &other)
{
    additionPatterns.rows.insert(additionPatterns.rows.end()
        , other.additionPatterns.rows.begin(), other.additionPatterns.rows.end());
        
    removalPatterns.rows.insert(removalPatterns.rows.end()
        , other.removalPatterns.rows.begin(), other.removalPatterns.rows.end());
}

void Change::join(Change &other)
{
    int leftColSize = headings.size();
    int rightColSize = other.headings.size();
    int newColSize = leftColSize + rightColSize - 1;
    
    for(Row &row: additionPatterns.rows)
    {
        int col = 0;
        string append;
        auto it = ++other.headings.begin();
        PathNode srcNode;
        srcNode.type = PathNode::NODE;
        while(col < newColSize - leftColSize)
        {
            
            PathNode edgeNode;
            edgeNode.id = it->id;
           
            if(it->dirType == Heading::DST)
            {
                append = append + "-[:" + it->id + "]->($)";
                edgeNode.type = PathNode::EDGE_RIGHT;
                if(it->id[it->id.size() - 1] == '*')
                {
                    edgeNode.maxLength = -1;
                }
                row.updatePath.push_back(edgeNode);
                row.updatePath.push_back(srcNode);
            }
            else
            {
                append = append + "<-[:" + it->id + "]-($)";
                edgeNode.type = PathNode::EDGE_LEFT;
                if(it->id[it->id.size() - 1] == '*')
                {
                    edgeNode.maxLength = -1;
                }
                row.updatePath.push_back(edgeNode);
                row.updatePath.push_back(srcNode);
            }
            col++;
            it++;
        }
        row.updateQuery = row.updateQuery + append;
        row.suffixQuery = row.suffixQuery + append;
    }
    
    for(Row &row: removalPatterns.rows)
    {
        int col = 0;
        string append;
        auto it = ++other.headings.begin();
        PathNode srcNode;
        srcNode.type = PathNode::NODE;
        while(col < newColSize - leftColSize)
        {
            PathNode edgeNode;
            edgeNode.id = it->id;
            
            if(it->dirType == Heading::DST)
            {
                append = append + "-[:" + it->id + "]->($)";
                edgeNode.type = PathNode::EDGE_RIGHT;
                if(it->id[it->id.size() - 1] == '*')
                {
                    edgeNode.maxLength = -1;
                }
                row.updatePath.push_back(edgeNode);
                row.updatePath.push_back(srcNode);
            }
            else
            {
                append = append + "<-[:" + it->id + "]-($)";
                edgeNode.type = PathNode::EDGE_LEFT;
                row.updatePath.push_back(edgeNode);
                row.updatePath.push_back(srcNode);
            }
            col++;
            it++;
        }
        row.updateQuery = row.updateQuery + append;
        row.suffixQuery = row.suffixQuery + append;
    }
    
    for(Row &row: other.additionPatterns.rows)
    {
        auto it = headings.begin();
        it++;
        int col = 0;
        string prepend;
        deque<PathNode> prependPath;
        PathNode srcNode;
        srcNode.type = PathNode::NODE;
        
        while(col < newColSize - rightColSize)
        {
            PathNode edgeNode;
            edgeNode.id = it->id;
            if(it->dirType == Heading::DST)
            {
                prepend = prepend + "($)-[:" + it->id + "]->";
                edgeNode.type = PathNode::EDGE_RIGHT;
                if(it->id[it->id.size() - 1] == '*')
                {
                    edgeNode.maxLength = -1;
                }
                prependPath.push_back(srcNode);
                prependPath.push_back(edgeNode);
            }
            else
            {
                prepend = prepend + "($)<-[:" + it->id + "]-";
                edgeNode.type = PathNode::EDGE_LEFT;
                if(it->id[it->id.size() - 1] == '*')
                {
                    edgeNode.maxLength = -1;
                }
                prependPath.push_back(srcNode);
                prependPath.push_back(edgeNode);
            }
            col++;
            it++;
        }
        row.updateQuery = prepend + row.updateQuery;
        
        for(int i = prependPath.size() - 1; i > -1; i--)
        {
             row.updatePath.push_front(prependPath[i]);
        }
        
        
        row.prefixQuery = prepend + row.prefixQuery;
    }
    
    for(Row &row: other.removalPatterns.rows)
    {
        auto it = headings.begin();
        it++;
        int col = 0;
        string prepend;
        deque<PathNode> prependPath;
        PathNode srcNode;
        srcNode.type = PathNode::NODE;
        while(col < newColSize - rightColSize)
        {
            PathNode edgeNode;
            edgeNode.id = it->id;
            
            if(it->dirType == Heading::DST)
            {
                prepend = prepend + "($)-[:" + it->id + "]->";
                edgeNode.type = PathNode::EDGE_RIGHT;
                if(it->id[it->id.size() - 1] == '*')
                {
                    edgeNode.maxLength = -1;
                }
                prependPath.push_back(srcNode);
                prependPath.push_back(edgeNode);
            }
            else
            {
                prepend = prepend + "($)<-[:" + it->id + "]-";
                edgeNode.type = PathNode::EDGE_LEFT;
                if(it->id[it->id.size() - 1] == '*')
                {
                    edgeNode.maxLength = -1;
                }
                prependPath.push_back(srcNode);
                prependPath.push_back(edgeNode);
            }
            col++;
            it++;
        }
        row.updateQuery = prepend + row.updateQuery;
        for(int i = prependPath.size() - 1; i > -1; i--)
        {
             row.updatePath.push_front(prependPath[i]);
        }
        row.prefixQuery = prepend + row.prefixQuery;
    }
    
    addAll(other);
    //Headings Stuff
    headings.insert(headings.end(), ++other.headings.begin(), other.headings.end());
}

void Change::_union(const Change &other)
{
    addAll(other);
    
    //Headings Stuff
    auto it = headings.begin();
    for(const auto &otherIt : other.headings)
    {
        it->id = it->id + "|" + otherIt.id;
        it++;
    }
}

void Change::prependHeading(const Heading &str)
{
    headings.push_front(str);
}

void Change::appendHeading(const Heading &str)
{
    headings.push_back(str);
}

void Change::prependToPatterns(const DataItem &str)
{
    
    for(auto &it : additionPatterns.rows)
    {
        it.prependColumn(str);
    }
    
    for(auto &it : removalPatterns.rows)
    {
        it.prependColumn(str);
    }
}

void Change::replaceColumn(int col, const DataItem &data)
{
    for(Row &it : additionPatterns.rows)
    {
        it.replaceColumn(col, data);
    }
    
    for(Row &it : removalPatterns.rows)
    {
        it.replaceColumn(col, data);
    }
}

void Change::reverseHeading()
{
    reverse(headings.begin(), headings.end());
}

void Change::appendToPatterns(const DataItem &str)
{
    for(auto &it : additionPatterns.rows)
    {
        it.appendColumn(str);
    }
    
    for(auto &it : removalPatterns.rows)
    {
        it.appendColumn(str);
    }
}

void Change::addAddition(const Row &row)
{
    additionPatterns.addRow(row);
}

void Change::addRemoval(const Row &row)
{
    removalPatterns.addRow(row);
}

void Row::appendColumn(const DataItem &newCol)
{
    columns.push_back(newCol);
}

void Row::prependColumn(const DataItem &newCol)
{
    columns.push_front(newCol);
}

void Row::replaceColumn(int col, const DataItem &newCol)
{
    if(col >= 0 && col < columns.size())
    {
        auto it = columns.begin();
        while(col > 0)
        {
            it++;
            col--;
        }
        *it = newCol;
    }

}

void Row::reverse()
{
    std::reverse(columns.begin(), columns.end());
    std::reverse(updatePath.begin(), updatePath.end());
    
    for(int i = 0; i < updatePath.size(); i++)
    {
        if(updatePath[i].type == PathNode::EDGE_RIGHT)
        {
            updatePath[i].type = PathNode::EDGE_LEFT;
        }
        else if(updatePath[i].type == PathNode::EDGE_LEFT)
        {
            updatePath[i].type = PathNode::EDGE_RIGHT;
        }
    }
    
}


Row Row::getPrefix(int startIndex) const
{
    Row result;
    int iters = startIndex;
    for(int i = 0; i <= startIndex; i++)
    {
        result.appendColumn(getCol(i));
    }
    return result;
}


int Row::getPrefixStart() const
{
    for(int prefixStart = 0; prefixStart < size(); prefixStart++)
    {
        if(getCol(prefixStart).id != "?")
        {
            return prefixStart;
        }
    }
    
    return -1;
}

int Row::getSuffixStart() const
{
    for(int suffixStart = size() - 1; suffixStart > -1; suffixStart--)
    {
        if(getCol(suffixStart).id != "?")
        {
            return suffixStart;
        }
    }
    
    return -1;
}

Row Row::getSuffix(int endIndex) const
{
    Row result;
    auto it = end(); it--;
    for(int i = endIndex; i < columns.size(); i++)
    {
        result.appendColumn(getCol(i));
    }
    return result;
}

Row Row::getRange(int startIndex, int endIndex) const
{
    Row result;
    for(startIndex; startIndex <= endIndex; startIndex++)
    {
        result.appendColumn(getCol(startIndex));
    }
    return result;
}

const DataItem& Row::getCol(int index) const
{
    if(index >= 0 && index < columns.size())
    {
        return columns[index];
    }
    throw "Index Out of Bounds";
    //~ return DataItem();
}

DataItem& Row::getCol(int index)
{
    if(index >= 0 && index < columns.size())
    {
        return columns[index];
    }
    throw "Index Out of Bounds";
    //~ return DataItem();
}

Row::row_iterator Row::begin()
{
    return columns.begin();
}

Row::const_row_iterator Row::begin() const
{
    return columns.cbegin();
}

Row::row_iterator Row::end()
{
    return columns.end();
}

Row::const_row_iterator Row::end() const
{
    return columns.cend();
}

void PatternTable::addRow(const Row &row)
{
   rows.push_back(row);
}

PatternTable::table_iterator PatternTable::begin()
{
    return rows.begin();
}

PatternTable::const_table_iterator PatternTable::begin() const
{
    return rows.begin();
}

PatternTable::table_iterator PatternTable::end()
{
    return rows.end();
}

PatternTable::const_table_iterator PatternTable::end() const
{
    return rows.end();
}

PatternTable& Change::getAdditionPatterns()
{
    return additionPatterns;
}

PatternTable& Change::getRemovalPatterns()
{
    return removalPatterns;
}

const PatternTable& Change::getAdditionPatterns() const
{
    return additionPatterns;
}

const PatternTable& Change::getRemovalPatterns() const
{
    return removalPatterns;
}


Row& Change::getAdditionPattern(int index)
{
    return additionPatterns[index];
}

Row& Change::getRemovalPattern(int index)
{
    return removalPatterns[index];
}


int Change::getNumHeadings()
{
    return headings.size();
}

Heading Change::getHeading(int index)
{
    return headings[index];
}

void Change::setHeading(int index, const Heading &str)
{
    headings[index] = str;
}


ostream& operator<<(ostream &out, const Change &change)
{
    for(const auto &it : change.headings)
    {
        out << it.id << " | ";
    }
    out << endl;
    out << "===Addition Patterns===" << endl;
    for(const auto &it : change.additionPatterns.rows)
    {
        out << it << endl;
    }
    
    out << "===Removal Patterns===" << endl;
    for(const auto &it : change.removalPatterns.rows)
    {
        out << it << endl;
    }
   
    return out;
}

std::ostream& operator<<(std::ostream &out, const Row &row)
{
    for(auto it = row.begin(); it != row.end(); it++)
    {
        if(it->transType == DataItem::TransType::DST)
        {
            out << it->id << "+ ";
        }
        else if(it->transType == DataItem::TransType::SRC)
        {
            out << "+" << it->id << " ";
        }
        else
        {
            out << it->id << " ";
        }
        
    }
    out << endl << "Query: " << row.updateQuery << endl;
    return out;
    
}
