
#include "ChangeHandler.h"
#include "Change.h"
#include "DependencyNode.h"
#include "QueryGenerator.h"

#include <boost/algorithm/string/replace.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>

#include <chrono>
#include <thread>
#include <mutex>
#include <algorithm>

using namespace std;
namespace fs = boost::filesystem;
using namespace std::chrono;

void getInOrder(const DependencyNode* root, vector<const DependencyNode*> &result)
{
    if(root)
    {
        getInOrder(root->getLeftChild(), result);
        result.push_back(root);
        getInOrder(root->getRightChild(), result);
    }
    
}


ChangeHandler::ChangeHandler(const DependencyNode* root, ostream &log) : log{log}
{
    vector<const DependencyNode*> inOrderTree;
    getInOrder(root, inOrderTree);
    string append;
    for(int i = 0; i < inOrderTree.size(); i++)
    {
        const DependencyNode *currNode = inOrderTree[i];
        preparedQuery = preparedQuery +  currNode->getCypher();
        currNode->handlePathNode(preparedPath);
        if(i < inOrderTree.size() - 1 
            && dynamic_cast<const JoinNode*>(currNode)
            && !dynamic_cast<const InverseNode*>(inOrderTree[i+1]))
        {
            preparedQuery = preparedQuery + "-[";
        }
        
        if(dynamic_cast<const JoinNode*>(currNode) || i == inOrderTree.size() - 1)
        {
            append = "]->";
        }
        
        if(dynamic_cast<const InverseNode*>(currNode))
        {
            append = "]-";
        }
        
    }
   
    if(preparedQuery[0] != '<')
    {
        preparedQuery = "-[" + preparedQuery;
    }
    preparedQuery = "MATCH ($)" + preparedQuery + append + "($) RETURN *";
    cerr << preparedQuery << endl;
    
}

void ChangeHandler::handleRemovals(const PatternTable &removals, neo4j_connection_t *connection, const string &neo4j_dbURL, const std::vector<int> &proj, const string &derivedFact_id)
{
    high_resolution_clock::time_point beginTime;
    unordered_set<Row> candidateRemovals;
    log << "Number of Removal Diffs," << removals.size() << endl;
    beginTime = high_resolution_clock::now();
    createDummyChange(removals, connection);
    log << "Creating Dummies (s)," << duration_cast<duration<double>>(high_resolution_clock::now() - beginTime).count() << endl;
    
    int numWorkers = thread::hardware_concurrency() - 1;
    mutex result_mutex;
    cerr << "Getting Removals..." << endl;
    beginTime = high_resolution_clock::now();
    process_dQueries<PatternTable::const_table_iterator>(removals.begin(), removals.end(), numWorkers, neo4j_dbURL,
    [this, &proj, &candidateRemovals, &result_mutex](neo4j_connection_t* workerConnection,const Row &dQuery){
        executeDeltaQuery(dQuery, proj, candidateRemovals, result_mutex, workerConnection);    
    });
    log << "Getting Removals (s)," << duration_cast<duration<double>>(high_resolution_clock::now() - beginTime).count() << endl;
    
    beginTime = high_resolution_clock::now();
    cleanUpDummies(connection);
    log << "Removing Dummies (s)," << duration_cast<duration<double>>(high_resolution_clock::now() - beginTime).count() << endl;
    log << "Initial Removals," << candidateRemovals.size() << endl;
    cerr << "Checking for Over-deletes..." << endl;
    fs::path tmpRemovals("removals.csv");
    fs::ofstream removalsWriter(tmpRemovals);
    removalsWriter << "n1";
    for(int i = 1; i < proj.size(); i++)
    {
        removalsWriter << ",n" << to_string(i+1);
    }
    removalsWriter << endl;
    int numRemovals = candidateRemovals.size();
    
    mutex writer_mutex;
    beginTime = high_resolution_clock::now();
    process_dQueries<unordered_set<Row>::const_iterator>(candidateRemovals.cbegin(), candidateRemovals.cend(), numWorkers, neo4j_dbURL,
        [this, &proj, &result_mutex, &writer_mutex, &numRemovals, &removalsWriter](neo4j_connection_t *workerConnection, const Row &dQuery)
    {
                
        if(!reInsert(dQuery, proj, workerConnection))
        {
            writer_mutex.lock();
            removalsWriter << "\"" << dQuery[0].id << "\"";
            for(int i = 1; i < dQuery.size(); i++)
            {
                removalsWriter << ",\"" << dQuery[i].id << "\"";
            }
            removalsWriter << endl;
            writer_mutex.unlock();
        }
        else
        {
            result_mutex.lock();
            numRemovals--;
            result_mutex.unlock();
        }
        
    });
    log << "Overdelete Check (s)," << duration_cast<duration<double>>(high_resolution_clock::now() - beginTime).count() << endl;
    candidateRemovals.clear();
    beginTime = high_resolution_clock::now();
    string batchRemoveQuery = "call apoc.periodic.iterate('LOAD CSV WITH HEADERS FROM \"file://" + fs::absolute(tmpRemovals).string() + "\" as row Return row',"
                  "'Match (m:DerivedFact {" + derivedFact_id + "}) delete m', {batchSize:1000, iterateList:true, parallel:true})";
    neo4j_close_results(neo4j_send(connection, batchRemoveQuery.c_str(), neo4j_null));
    log << "Applying Removals (s)," << duration_cast<duration<double>>(high_resolution_clock::now() - beginTime).count() << endl;
    log << "Removals Made," << numRemovals << endl;
}

void ChangeHandler::handleAdditions(const PatternTable &adds, neo4j_connection_t *connection, const string &neo4j_dbURL, const std::vector<int> &proj, const string &derivedFact_id)
{
    high_resolution_clock::time_point beginTime;
    log << "Number of Dirty Diffs," << adds.size() << endl;
    unordered_set<Row> additions;
    beginTime = high_resolution_clock::now();
    fs::path tmpAdditions("additions.csv");
    fs::ofstream additionsWriter(tmpAdditions);
    additionsWriter << "n1";
    for(int i = 1; i < proj.size(); i++)
    {
        additionsWriter << ",n" << to_string(i+1);
    }
    additionsWriter << endl;

    int numWorkers = thread::hardware_concurrency() - 1;
    mutex result_mutex;
    process_dQueries<PatternTable::const_table_iterator>(adds.begin(), adds.end(), numWorkers, neo4j_dbURL,
    [this, &proj, &additions, &result_mutex](neo4j_connection_t* workerConnection,const Row &dQuery){
        executeDeltaQuery(dQuery, proj, additions, result_mutex, workerConnection);    
    });
    for (const Row &add : additions)
    {
      additionsWriter << "\"" << add[0].id << "\"";
      for(int i = 1; i < add.size(); i++)
      {
        additionsWriter << ",\"" << add[i].id << "\"";
      }
      additionsWriter << endl;
    }

    log << "Getting Additions (s)," << duration_cast<duration<double>>(high_resolution_clock::now() - beginTime).count() << endl;
    int numAdditions = additions.size();
    additions.clear();
    beginTime = high_resolution_clock::now();
    string batchMergeQuery = "call apoc.periodic.iterate('LOAD CSV WITH HEADERS FROM \"file://" + fs::absolute(tmpAdditions).string() + "\" as row Return row',"
                  "'Merge (m:DerivedFact {" + derivedFact_id + "})', {batchSize:1000, iterateList:true, parallel:true})";
    neo4j_close_results(neo4j_send(connection, batchMergeQuery.c_str(), neo4j_null));
    log << "Applying Additions (s)," << duration_cast<duration<double>>(high_resolution_clock::now() - beginTime).count() << endl;
    log << "Additions Made," << numAdditions << endl;
}

void ChangeHandler::handleChange(const Change &change, neo4j_connection_t *connection, const string &neo4j_dbURL, const std::vector<int> &proj)
{
    // Get Removals First
    string derivedFact_id = "n" + to_string(1) + ":row.n1";
    for(int i = 1; i < proj.size(); i++)
    {
        derivedFact_id = derivedFact_id + ", n" + to_string(i+1) + ":row.n" + to_string(i+1);
    }
    cerr << derivedFact_id << endl;
    
    handleRemovals(change.getRemovalPatterns(), connection, neo4j_dbURL, proj, derivedFact_id);
    handleAdditions(change.getAdditionPatterns(), connection, neo4j_dbURL, proj, derivedFact_id);
    
}

void ChangeHandler::executeDeltaQuery(const Row &currChange, const vector<int> &proj, unordered_set<Row> &results, mutex &m, neo4j_connection_t *connection)
{
    string query = DeltaQueryGenerator{}.generateQuery(currChange.updatePath);
    vector<string> nodeQuery, transNodes;
    bool trans = false;
    int numNodes = 0;
    for(int i = 0; i < currChange.updatePath.size(); i++)
    {
        if(currChange.updatePath[i].type == PathNode::NODE)
        {
            if(!currChange.updatePath[i].id.empty())
            {
                string tmp = "(n" + to_string(numNodes + 1) + ":BaseFact {id:'" + currChange.updatePath[i].id + "'})";
                nodeQuery.push_back(tmp);
                transNodes.push_back(currChange.updatePath[i].id);
            }
            numNodes++;
        }
        else if(currChange.updatePath[i].minLength == 0)
        {
            trans = true;
        }
    }
    string queryPrepend = "MATCH " + nodeQuery[0] + "\n"
                        + "MATCH " + nodeQuery[1]+ "\n";                   
    string return_ = "n1";
    for(int i = 2; i <= numNodes; i++)
    {
        return_ = return_ + ",n" + to_string(i);
    }
    // Replacing Placeholders in prepared statement
    query = queryPrepend + query + " RETURN " + return_;
    // log << query << endl;
    neo4j_result_stream_t *paths =
        neo4j_run(connection, query.c_str(), neo4j_null);
    if (paths == NULL)
    {
        cout << "Error with FactBase Update!" << endl;
    }
    else
    {
        int numFacts = 0;
	    int numCols = neo4j_nfields(paths);
        neo4j_result_t *path = neo4j_fetch_next(paths);
        while(path != NULL)
        {
            Row result;
            unordered_multiset<string> transCheck;
            transCheck.insert(transNodes[0]);
            transCheck.insert(transNodes[1]);
            for(int i = 0; i < numCols; i++)
            {
                char buf[2048];
                neo4j_value_t node = neo4j_result_field(path, i);
                neo4j_value_t id = neo4j_map_get(neo4j_node_properties(node), "id");
                neo4j_string_value(id, buf, sizeof(buf));
                string idAsString(buf);
                DataItem d;
                d.id = idAsString;
                auto it = transCheck.find(d.id);
                if(trans && it != transCheck.end())
                {
                    transCheck.erase(it);
                }
                else
                {
                    result.appendColumn(d);
                }
                
            }
            assert(result.size() == proj.size());
            numFacts++;
            m.lock();
            results.insert(result);
            m.unlock();
            path = neo4j_fetch_next(paths);
        }
	    // m.lock();
	    // log << currChange << "," << numFacts << endl;
	    // m.unlock();
    }
    
}


void ChangeHandler::createDummyChange(const PatternTable &removals, neo4j_connection_t *connection)
{
    for(const Row &removal : removals)
    {
        string query = "Merge (src:BaseFact {id:\"" + removal.dummySrc + "\"}) \n"
                     + "ON CREATE SET src:BaseFact:DUMMY \n"
                     + "Merge (dst:BaseFact {id:\"" + removal.dummyDst + "\"}) \n"
                     + "ON CREATE SET dst:BaseFact:DUMMY \n"
                     + "Merge (src)-[r:" + removal.dummyRelType + "]->(dst) \n"
                     + "ON CREATE SET r.dummy=true";
        neo4j_result_stream_t *result = neo4j_send(connection, query.c_str(), neo4j_null);
        neo4j_close_results(result);
    }
}

void ChangeHandler::cleanUpDummies(neo4j_connection_t *connection)
{
    cerr << "Removing Dummy Nodes" << endl;
    string query = "MATCH (n:DUMMY) detach delete n";
    neo4j_result_stream_t *result = neo4j_send(connection, query.c_str(), neo4j_null);
    neo4j_close_results(result);
    cerr << "Removing Dummy Edges" << endl;
    query = "MATCH ()-[r {dummy:true}]-() delete r";
    result = neo4j_send(connection, query.c_str(), neo4j_null);
    neo4j_close_results(result);

}



bool ChangeHandler::reInsert(const Row &r, const vector<int> &proj, neo4j_connection_t *connection)
{
    // Prepare Parameterized Query to Execute
    string query = LocalisedQueryGenerator{proj}.generateQuery(preparedPath);
    vector<string> nodeQuery;
    for(int i = 0, projIndex = 0, numNodes = 0; i < preparedPath.size(); i++)
    {
        if(preparedPath[i].type == PathNode::NODE)
        {
            if(projIndex < proj.size() && numNodes == proj[projIndex])
            {
                string tmp = "(n" + to_string(numNodes + 1) + ":BaseFact {id:'" + r[projIndex].id + "'})";
                nodeQuery.push_back(tmp);
                projIndex++;
            }
            numNodes++;
        }
    }
    string prepend;
    for(const string &node : nodeQuery)
    {
        prepend = prepend + "MATCH " + node + "\n";
    } 
    query = prepend + query + " Return true";
    
    
    // log << query << endl;
    neo4j_result_stream_t *paths =
        neo4j_run(connection, query.c_str(), neo4j_null);
    if (paths == NULL)
    {
        cout << "Error with FactBase Update!" << endl;
    }
    else
    {
        high_resolution_clock::time_point beginTime = high_resolution_clock::now();
        neo4j_result_t *path = neo4j_fetch_next(paths);
        if(path != NULL)
        {
            return true;
        }
        
    }
    return false;
    
}