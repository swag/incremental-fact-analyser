#!/bin/bash

datasetDir=$1
cd ~/Rex-Build
rm -rf correctness
mkdir correctness
logFile=$2
> $logFile
for i in 20 25 30 35 40 45 50 55 60 65 70 75 80 85 90 95 100
do
    echo "===========Running with change limit: $i============" >> $logFile
    for j in 0 1
    do
        sudo neo4j-admin import --nodes="$datasetDir/nodes.csv" --relationships="$datasetDir/edges.csv" --delimiter="\t" --database="linux.db"
        sudo neo4j start
        dbDown=1
        while [ $dbDown -ne 0 ]
        do
            sleep 5
            curl --user neo4j:1375Dave! http://localhost:7474/
            dbDown=$?
        done
        echo "Setting up initial Result Set..."
        python3 ./transCallsExample/pyNeo4j.py
        sudo neo4j stop
        sudo chmod 777 -R ~/Neo4J_DBs
        echo "Applying Incremental Update..."
        java -jar ~/IdeaProjects/neo4j-incremental/updater/target/incremental-1.0.0-jar-with-dependencies.jar $datasetDir/diffs /home/danbarna/Neo4J_DBs/data/databases/linux.db $i
        sudo neo4j start
        dbDown=1
        while [ $dbDown -ne 0 ]
        do
            sleep 5
            curl --user neo4j:1375Dave! http://localhost:7474/
            dbDown=$?
        done
        if [ "$j" == "0" ]
        then
            echo "---Using FactBase---" >> $logFile
        else
            echo "---Using ResultSet---" >> $logFile
        fi
        
        echo "Running Incremental Analysis..."
        ./transCallsExample/cypherParse $j >> $logFile
        
        if [ "$j" == "0" ]
        then
            mv actual.csv correctness/actual_factBase_$i.csv
        else
            mv actual.csv correctness/actual_resultSet_$i.csv
        fi
        
        mv expected.csv correctness/expected_$i.csv
        sudo neo4j stop
        sudo rm -rf ~/Neo4J_DBs/data/databases/linux.db/
    done
done
